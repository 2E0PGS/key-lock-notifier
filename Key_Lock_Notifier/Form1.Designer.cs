﻿namespace Key_Lock_Notifier
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.lblTkeylocknotifier = new System.Windows.Forms.Label();
            this.gbOptions = new System.Windows.Forms.GroupBox();
            this.cbInsertkey = new System.Windows.Forms.CheckBox();
            this.cbNumlock = new System.Windows.Forms.CheckBox();
            this.cbCaplock = new System.Windows.Forms.CheckBox();
            this.btnQuit = new System.Windows.Forms.Button();
            this.cbEnablesounds = new System.Windows.Forms.CheckBox();
            this.gbExtraoptions = new System.Windows.Forms.GroupBox();
            this.cbEnablepopups = new System.Windows.Forms.CheckBox();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.gbOptions.SuspendLayout();
            this.gbExtraoptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTkeylocknotifier
            // 
            this.lblTkeylocknotifier.AutoSize = true;
            this.lblTkeylocknotifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTkeylocknotifier.Location = new System.Drawing.Point(74, 9);
            this.lblTkeylocknotifier.Name = "lblTkeylocknotifier";
            this.lblTkeylocknotifier.Size = new System.Drawing.Size(127, 20);
            this.lblTkeylocknotifier.TabIndex = 0;
            this.lblTkeylocknotifier.Text = "Key Lock Notifier";
            // 
            // gbOptions
            // 
            this.gbOptions.Controls.Add(this.cbInsertkey);
            this.gbOptions.Controls.Add(this.cbNumlock);
            this.gbOptions.Controls.Add(this.cbCaplock);
            this.gbOptions.Location = new System.Drawing.Point(12, 32);
            this.gbOptions.Name = "gbOptions";
            this.gbOptions.Size = new System.Drawing.Size(133, 133);
            this.gbOptions.TabIndex = 1;
            this.gbOptions.TabStop = false;
            this.gbOptions.Text = "Options";
            // 
            // cbInsertkey
            // 
            this.cbInsertkey.AutoSize = true;
            this.cbInsertkey.Checked = true;
            this.cbInsertkey.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbInsertkey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbInsertkey.Location = new System.Drawing.Point(7, 68);
            this.cbInsertkey.Name = "cbInsertkey";
            this.cbInsertkey.Size = new System.Drawing.Size(73, 17);
            this.cbInsertkey.TabIndex = 2;
            this.cbInsertkey.Text = "Insert Key";
            this.cbInsertkey.UseVisualStyleBackColor = true;
            this.cbInsertkey.CheckedChanged += new System.EventHandler(this.cbInsertkey_CheckedChanged);
            // 
            // cbNumlock
            // 
            this.cbNumlock.AutoSize = true;
            this.cbNumlock.Checked = true;
            this.cbNumlock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbNumlock.Location = new System.Drawing.Point(7, 44);
            this.cbNumlock.Name = "cbNumlock";
            this.cbNumlock.Size = new System.Drawing.Size(75, 17);
            this.cbNumlock.TabIndex = 1;
            this.cbNumlock.Text = "Num Lock";
            this.cbNumlock.UseVisualStyleBackColor = true;
            this.cbNumlock.CheckedChanged += new System.EventHandler(this.cbNumlock_CheckedChanged);
            // 
            // cbCaplock
            // 
            this.cbCaplock.AutoSize = true;
            this.cbCaplock.Checked = true;
            this.cbCaplock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCaplock.Location = new System.Drawing.Point(7, 20);
            this.cbCaplock.Name = "cbCaplock";
            this.cbCaplock.Size = new System.Drawing.Size(72, 17);
            this.cbCaplock.TabIndex = 0;
            this.cbCaplock.Text = "Cap Lock";
            this.cbCaplock.UseVisualStyleBackColor = true;
            this.cbCaplock.CheckedChanged += new System.EventHandler(this.cbCaplock_CheckedChanged);
            // 
            // btnQuit
            // 
            this.btnQuit.Location = new System.Drawing.Point(197, 184);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(75, 23);
            this.btnQuit.TabIndex = 3;
            this.btnQuit.Text = "Quit";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // cbEnablesounds
            // 
            this.cbEnablesounds.AutoSize = true;
            this.cbEnablesounds.Checked = true;
            this.cbEnablesounds.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEnablesounds.Location = new System.Drawing.Point(16, 19);
            this.cbEnablesounds.Name = "cbEnablesounds";
            this.cbEnablesounds.Size = new System.Drawing.Size(98, 17);
            this.cbEnablesounds.TabIndex = 3;
            this.cbEnablesounds.Text = "Enable Sounds";
            this.cbEnablesounds.UseVisualStyleBackColor = true;
            this.cbEnablesounds.CheckedChanged += new System.EventHandler(this.cbEnablesounds_CheckedChanged);
            // 
            // gbExtraoptions
            // 
            this.gbExtraoptions.Controls.Add(this.cbEnablepopups);
            this.gbExtraoptions.Controls.Add(this.cbEnablesounds);
            this.gbExtraoptions.Location = new System.Drawing.Point(152, 33);
            this.gbExtraoptions.Name = "gbExtraoptions";
            this.gbExtraoptions.Size = new System.Drawing.Size(120, 132);
            this.gbExtraoptions.TabIndex = 4;
            this.gbExtraoptions.TabStop = false;
            this.gbExtraoptions.Text = "Extra Options";
            // 
            // cbEnablepopups
            // 
            this.cbEnablepopups.AutoSize = true;
            this.cbEnablepopups.Checked = true;
            this.cbEnablepopups.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEnablepopups.Location = new System.Drawing.Point(16, 43);
            this.cbEnablepopups.Name = "cbEnablepopups";
            this.cbEnablepopups.Size = new System.Drawing.Size(98, 17);
            this.cbEnablepopups.TabIndex = 4;
            this.cbEnablepopups.Text = "Enable Popups";
            this.cbEnablepopups.UseVisualStyleBackColor = true;
            this.cbEnablepopups.CheckedChanged += new System.EventHandler(this.cbEnablepopups_CheckedChanged);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "Program Minimised to Tray";
            this.notifyIcon1.BalloonTipTitle = "Key Lock Notifier";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Key Lock Notifier";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 218);
            this.Controls.Add(this.gbExtraoptions);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.gbOptions);
            this.Controls.Add(this.lblTkeylocknotifier);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Key Lock Notifier";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.gbOptions.ResumeLayout(false);
            this.gbOptions.PerformLayout();
            this.gbExtraoptions.ResumeLayout(false);
            this.gbExtraoptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTkeylocknotifier;
        private System.Windows.Forms.GroupBox gbOptions;
        private System.Windows.Forms.CheckBox cbInsertkey;
        private System.Windows.Forms.CheckBox cbNumlock;
        private System.Windows.Forms.CheckBox cbCaplock;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.CheckBox cbEnablesounds;
        private System.Windows.Forms.GroupBox gbExtraoptions;
        private System.Windows.Forms.CheckBox cbEnablepopups;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Timer timer1;
    }
}

