﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Key_Lock_Notifier.Properties; //Allows us to store user settings in appdata. 

/*
 * Programmed by: Peter Stevenson (2E0PGS)
 * Licence: GNU General Public Licence
 * Simple tool for Windows that notifies user when:
 * -Caps Lock Enabled
 * -Number Lock Disabled
 * -Insert Key Enabled
 * Tested On: Windows 7, and Windows 8
 * Should Work On: Windows 7 or Newer
 * Requires .NET Framework 4.5
 */

/*
    Copyright (C) <2015>  <Peter Stevenson>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Key_Lock_Notifier
{
    public partial class Form1 : Form
    {
        Boolean SoundEnabled = true;
        Boolean PopupsEnabled = true;
        Boolean soundplayedCL = false;
        Boolean soundplayedNL = false;
        Boolean soundplayedIK = false;
        Boolean ShownPopupCL = false;
        Boolean ShownPopupNL = false;
        Boolean ShownPopupIK = false;

        public Form1()
        {
            InitializeComponent();            
        }

        private void Form1_Load(object sender, EventArgs e) //Load our settings when program loads
        {
            cbCaplock.Checked = Settings.Default.Caplock;
            cbNumlock.Checked = Settings.Default.Numlock;
            cbInsertkey.Checked = Settings.Default.Insert;
            cbEnablesounds.Checked = Settings.Default.Sounds;
            cbEnablepopups.Checked = Settings.Default.Popups;
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.BalloonTipText = "Program Minimised to Tray";
                notifyIcon1.ShowBalloonTip(1000);
                this.ShowInTaskbar = false;
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            check_keys();
        }

        private void check_keys()
        {            
            if (cbCaplock.Checked == true)
            {                
                if (Control.IsKeyLocked(Keys.CapsLock))
                {                    
                    if (PopupsEnabled == true)
                    {
                        if (ShownPopupCL == false)
                        {
                            notifyIcon1.BalloonTipText = "Caps Lock Enabled";
                            notifyIcon1.ShowBalloonTip(1000);
                            ShownPopupCL = true;
                        }
                    }                    
                    if(SoundEnabled == true)
                    {
                        if (soundplayedCL == false)
                        {
                            System.Media.SystemSounds.Exclamation.Play();
                            soundplayedCL = true;
                        }
                    }
                }
                else
                {
                    soundplayedCL = false;
                    ShownPopupCL = false;
                }
            }
            if (cbNumlock.Checked == true)
            {                
                if (Control.IsKeyLocked(Keys.NumLock))
                {
                    soundplayedNL = false;
                    ShownPopupNL = false;
                }
                else
                {
                    if (PopupsEnabled == true)
                    {
                        if (ShownPopupNL == false)
                        {
                            notifyIcon1.BalloonTipText = "Num Lock Disabled";
                            notifyIcon1.ShowBalloonTip(1000);
                            ShownPopupNL = true;
                        }
                    }
                    if (SoundEnabled == true)
                    {
                        if (soundplayedNL == false)
                        {
                            System.Media.SystemSounds.Exclamation.Play();
                            soundplayedNL = true;
                        }
                    }
                }
            }
            if (cbInsertkey.Checked == true)
            {
                if (Control.IsKeyLocked(Keys.Insert))
                {
                    if (PopupsEnabled == true)
                    {
                        if (ShownPopupIK == false)
                        {
                            notifyIcon1.BalloonTipText = "Insert Key Enabled";
                            notifyIcon1.ShowBalloonTip(1000);
                            ShownPopupIK = true;
                        }
                    }
                    if (SoundEnabled == true)
                    {
                        if (soundplayedIK == false)
                        {
                            System.Media.SystemSounds.Exclamation.Play();
                            soundplayedIK = true;
                        }
                    }
                }
                else
                {
                    soundplayedIK = false;
                    ShownPopupIK = false;
                }
            }
        }//end of check keys

        private void cbEnablesounds_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.Sounds = cbEnablesounds.Checked;
            Settings.Default.Save();
            if(cbEnablesounds.Checked == true)
            {
                SoundEnabled = true;
            }
            else
            {
                SoundEnabled = false;
            }
        }

        private void cbEnablepopups_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.Popups = cbEnablepopups.Checked;
            Settings.Default.Save();
            if(cbEnablepopups.Checked == true)
            {
                PopupsEnabled = true;                
            }
            else
            {
                PopupsEnabled = false;
            }
        }        

        private void cbCaplock_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.Caplock = cbCaplock.Checked;
            Settings.Default.Save();
        }

        private void cbNumlock_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.Numlock = cbNumlock.Checked;
            Settings.Default.Save();
        }

        private void cbInsertkey_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.Insert = cbInsertkey.Checked;
            Settings.Default.Save();
        }             

        
    }//end of form1
}//end of namespace
