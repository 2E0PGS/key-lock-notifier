############################################################################################
Simple tool for Windows that notifies user when:
*Caps Lock Enabled
*Number Lock Disabled
*Insert Key Enabled

Features:
*Sound Notification
*Pop-up Balloon Notification 
*Minimises to tray
*Option to disable certain features

Operating System Support:
*Tested on Windows 7 and Windows 8
*Should work on Windows 7 or Newer
*Microsoft .net Framework version 4.5

How do I download the .exe ?
*You need to find the download section. 
*Navigate to download section on the repository. 
*The Build depends on if I have compiled it recently. Which I do very often.
*Once you got the .exe file it will run from it like a portable program. NO INSTALL REQUIRED.
*However you do need Microsoft .net Framework 4.5

Start on boot ?
*Add a shortcut to the .exe in your start up folder.
*Change the properties of the shortcut to "start minimised". 

Licence:
*This program is licensed under GNU General Public Licence
*http://opensource.org/licenses/GPL-3.0

############################################################################################